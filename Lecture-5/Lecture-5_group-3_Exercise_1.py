### Exercise 1 of Lecture 5 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import numpy as np
import pandas as pd
import datetime as dt

## 1. Extracting information from a dataset

# a) Calculate moving time
df = pd.read_csv('./strava_uncleaned.csv')

# Function to get hours in float from a string
def get_hours(time_string):
    if pd.isnull(time_string): # Take NaN in consideration
        return np.nan
    hour, min, sec = [float(time) for time in time_string.split(':')]
    return hour + min / 60 + sec / 3600

# Test
print(get_hours('1:30:00')) # 1.5
print(get_hours('02:45:11')) # 2.7530555555555556

print(70 * '-')

# Create new column moving_time_h and apply function to entire column
df['moving_time_h'] = df['moving_time'].apply(get_hours)

mean_moving_time = df['moving_time_h'].mean()
print(f'Mean moving time: {mean_moving_time:.3f} hour(s)')
## Mean moving time: 0.849 hour(s)

moving_time_200 = df['moving_time_h'][199]
print(f'Moving time of the 200th run: {moving_time_200:.3f} hour(s)')
## Moving time of the 200th run: 0.722 hour(s)


# b) Estimate the length of each run
df['length_km'] = df['moving_time_h'] * df['average_speed_kmph']

mean_length = df['length_km'].mean()
print(f'Mean length of each run: {mean_length:.3f} km')
## Mean length of each run: 3.766 km

# c) Save the DataFrame

df.to_csv('strava_uncleaned_edit.csv')

