### Exercise 3 of Lecture 5 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import numpy as np
import pandas as pd

## --------------3. Concatenation and MultiIndexes------------------

# a) ----Concatenate DataFrame------

## read in datasets
df1 = pd.read_csv('./strava_uncleaned_edit.csv')
df2 = pd.read_csv('./strava_var_2.csv')

# merge the datasets
conc = pd.merge(df1, df2)

# b) ----Mean elevation gain for each year and place combination-----
# array of tuples
year_place = np.unique(conc[['place', 'year']].apply(tuple, axis=1).values)

# initialize empty array
elevation_gain = np.zeros(len(year_place))

# loop through the tuples
for i in range(len(year_place)):
    # extract year and place
    y = year_place[i][1]
    p = year_place[i][0]
    # get slice of dataframe
    smol = conc[(conc.year == y) & (conc.place == p)]
    # extract mean into elevation_gain array
    elevation_gain[i] = np.mean(smol.total_elevation_gain)

# c) --------MultiIndex-------
# Create a MultiIndex (dataframe I guess?)

multi = pd.MultiIndex.from_tuples(year_place)
print(multi)

# multi_df = pd.DataFrame(elevation_gain, index=multi, columns=["elevation_gain"])
# print(multi_df)

