### Exercise 2 of Lecture 5 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import pandas as pd

## 2. Estimate heart-rate

df = pd.read_csv('./strava_uncleaned_edit.csv')

df['average_heartrate'].fillna(123.3 + df['average_speed_kmph'] * 3.4 + df['moving_time_h'] * 10.5 - df['length_km'] * 2.4, inplace = True)

mean_average_hr = df['average_heartrate'].mean()
print(f'Mean average heartrate: {mean_average_hr:.3f}')
## Mean average heartrate: 139.339

