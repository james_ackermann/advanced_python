### Exercise 1 of Lecture 7 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

## 1. World happiness report
# Load datasets
df = pd.read_csv('./materials/world_happiness_report.csv')
region = pd.read_csv('./materials/region.csv')

# a) Regions
df = pd.merge(df, region[['name', 'region']], how='left', left_on='Country name', right_on='name').drop(columns ='name')

# df.to_csv('world_happiness_report_v3.csv')

# b) Life-expectancy
life_exp_mean = df.groupby(['region', 'year'])['Healthy life expectancy at birth'].mean()

# c) Pivot table for life-expectancy
life_exp_pivot = df.pivot_table('Healthy life expectancy at birth', index = 'year', columns = 'region')

# d) Pivot table for generosity
generosity_pivot = df.pivot_table(values = 'Generosity', index = ['region', 'Country name'], columns = 'year')

# e) Re-create the graph
life_exp_pivot.plot()

# Set limits of x axis
plt.xlim(2005, 2020)

# Set labels and legend
plt.ylabel('Healthy life expectancy at birth')
plt.xlabel('Year')
plt.legend(loc = 'lower right', title = 'region')

# Save figure to PDF format
plt.savefig('Regional_healthy_life_expectancy_per_year.pdf')
plt.show()
