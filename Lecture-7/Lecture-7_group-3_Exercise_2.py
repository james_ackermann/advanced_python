### Exercise 2 of Lecture 7 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import pandas as pd

## 2. Substrings
df = pd.read_csv('world_happiness_report_v3.csv')

# a) Masks
mask_N = df['Country name'].str.startswith('N')
mask_2017 = df['year'] == 2017

# Assign masks to new dataframe
df_N_2017 = df[mask_N & mask_2017]
country = df_N_2017['Country name']

print('Countries that start with "N" with values from 2017:')
print(country)

print(70 * '-')

# b) Length of strings
print(country.str.len())

print(70 * '-')

# c) Regular expression
regex_country = country.str.findall(r'[a-zA-Z ]+a$')
country_list = list(map(''.join, regex_country))

print([x for x in country_list if x != ''])
