### Exercise 3 of Lecture 7 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import numpy as np
import pandas as pd
import datetime as dt


## 3. Weather data

weather_data = pd.read_csv('./materials/uk_weather.csv')

# a) Datetime

# helper dataframe to feed into pd.datetime()
helper = weather_data[['year', 'month']].copy()
helper['day'] = 1

# add datetime column to original df
weather_data['datetime'] = pd.to_datetime(helper)
del helper
# b) type

print('The datatype of our datetime column is:', weather_data['datetime'].dtypes) # datetime64

# c) user query and response 1:
    
def to_datetime(date):
    date = date.split(' ')
    year = date[1]
    mymonth = date[0]
    mymonth = mymonth[0].upper()+mymonth[1:]
    mymonth = dt.datetime.strptime(mymonth, '%B').month
    # return dt.date(year, mymonth, 1) 
    return np.datetime64(f'{year}-'+'{:02d}'.format(mymonth))

  
station = input('Station > ').lower()
from_date = input('From Date > ').lower()
to_date = input('To Date > ').lower()

'''
station = 'manston'
from_date= 'january 2019'
to_date= 'march 2020'
'''

from_date = to_datetime(from_date)
to_date = to_datetime(to_date)

myslice = weather_data[(weather_data['datetime'] <= to_date) & (weather_data['datetime'] >= from_date) & (weather_data['station'] == station)]

mean_rainfall = np.mean(myslice['rain'])

print(f'Mean Rainfall: {mean_rainfall:.2f} mm')

# d) part 2:
    
max_rainday = myslice[myslice['rain']==np.max(myslice['rain'])]['datetime']
now = np.datetime64('today')
days_since = now - max_rainday
days_since = str(days_since).split()[1]

print(f'Days since maximum rainfall in this period: {days_since}')

















