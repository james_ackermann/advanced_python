### Exercise 3 of Lecture 4 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
import numpy as np
import pandas as pd

## 3. Series

# a) Create list from column values
df = pd.read_csv('./strava.csv')

data_list = sorted(df['moving_time_h'].head(5).values.tolist())
key_list = ['Shortest', 'Short', 'Medium', 'Long', 'Longest']

data_series = pd.Series(data_list, index = key_list)

# b) Read value
print(data_series['Medium'])

# c) Dictionaries
data_dict = dict(zip(key_list, data_list))
dict_series = pd.Series(data_dict)

# d) Index
data_dict = dict(zip(key_list, data_list))
dict_series = pd.Series(data_dict, index = ['Shortest', 'Longest'])