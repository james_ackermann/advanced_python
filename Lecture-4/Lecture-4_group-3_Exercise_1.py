### Exercise 1 of Lecture 4 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
from os import defpath
import numpy as np
import pandas as pd

## 1. Working with DataFrames

# a) Load the dataset and create column
df = pd.read_csv('./strava.csv')

# Create column and convert mph to kmph
df['average_speed_kmph'] = (1.60934 * df['average_speed_mph']).round(3)

# b) Length of each run
# Create column and convert to length of runs in km
df['length_km'] = (df['moving_time_h'] * df['average_speed_kmph']).round(2)

# Find and print mean length of all runs in km
mean_length_km = df['length_km'].mean()
print(f'The mean length of all runs: {mean_length_km:.2f} km')

# c) Saving
df.to_csv('./strava_v2.csv')

# d) Count number of entries fulfilling a condition
long_runs = df.loc[df['length_km'] > 5]
short_runs = df.loc[df['length_km'] <= 5]

print(f'Number of runs above 5 km: {long_runs.shape[0]}')
print(f'Number of runs at or below 5 km: {short_runs.shape[0]}')

# e) Find averages
mean_heart_long = long_runs['average_heartrate'].mean()
mean_heart_short = short_runs['average_heartrate'].mean()
print(f'Mean heart-rate for long runs: {mean_heart_long:.1f}')
print(f'Mean heart-rate for short runs: {mean_heart_short:.1f}')