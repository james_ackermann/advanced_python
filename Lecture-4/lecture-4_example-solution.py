'''
Lecture 4 Exercises - example solution.
Note that these examples only represent one of many acceptable ways of solving
the exercises, and the level of sophistication expected at this point in the
course. More sophisticated solutions are always appreciated but not expected.
'''


import pandas as pd



''' Exercise 1 '''

# Exercise 1a
df = pd.read_csv('./strava.csv')
df['average_speed_kmph'] = df['average_speed_mph'] * 1.60934

# Exercise 1b
df['length_km'] = df['average_speed_kmph'] * df['moving_time_h']
print('Mean length: {:.1f} km.'.format(df['length_km'].mean()))

# Exercise 1c
df.to_csv('strava_v2.csv')

# Exercise 1d
long_runs = df[df.length_km > 5 ]
short_runs = df[df.length_km <= 5]
print(' >5 KM runs, N = {0}\n<=5 KM runs, N = {1}'.format(
    long_runs.shape[0], short_runs.shape[0]))

# Exercise 1e
long_run_avg_hr = long_runs['average_heartrate'].mean()
short_run_avg_hr = short_runs['average_heartrate'].mean()
print(' >5 KM runs, M HR = {0:.1f}\n<=5 KM runs, M HR = {1:.1f}'.format(
    long_run_avg_hr, short_run_avg_hr))



''' Exercise 2 '''

# Exercise 2a
for _, row in df.loc[24:35].iterrows():
    print(f'Length of run = {row.length_km:06.2f}',
          f'elevation gain = {row.total_elevation_gain:06.2f}',
          f'gain per km = {row.total_elevation_gain / row.length_km:06.2f}',
          sep=', ')

# Exercise 2b
avg_hr = df.iloc[24:35].average_heartrate.mean()
print(f'M HR = : {avg_hr:.2f}')

# Exercise 2c
df['gain_per_km'] = df['total_elevation_gain'] / df['length_km']
max_ele_gain = df.iloc[24:34].gain_per_km.max()
hr_max_gain = float(df[df['gain_per_km'] == max_ele_gain].average_heartrate)
print('HR on run with max elevation gain: {0}, Higher than avg. HR: {1}'.format(
    hr_max_gain, hr_max_gain > avg_hr))



''' Exercise 3 '''

# Exercise 3a
dur_vals = sorted([df.loc[row, 'moving_time_h'] for row in range(5)])
dur_keys = ['Shortest', 'Short', 'Medium', 'Long', 'Longest']
dur_series = pd.Series(dur_vals, index=dur_keys)

# Exercise 3b
print('Duration of "Medium" run = {}'.format(dur_series['Medium']))

# Exercise 3c
dur_series = pd.Series(dict(zip(dur_keys, dur_vals)))

# Exercise 3d
dur_series = pd.Series(dict(zip(dur_keys, dur_vals)), index=dur_keys[::4])