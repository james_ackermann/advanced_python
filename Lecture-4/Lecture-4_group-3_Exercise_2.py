### Exercise 3 of Lecture 4 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

##----- load packages
import numpy as np
import pandas as pd

# Load the dataset
df = pd.read_csv('./strava_v2.csv')

### 2. Working with values stored in a DataFrame

## a) print selected data from 25th to 35th row

print(df.columns)

for i in range(24, 35):
    lrun = df['length_km'][i]
    elev_gain = df['total_elevation_gain'][i]
    gain_km = elev_gain/lrun
    print(f'Length of run = {lrun}, elevation gain = {elev_gain}, gain per km = {gain_km:.2f}')


## b) average heart rate in selected range

avgheart = df['average_heartrate'][24:35].mean() #137.84
avgheart2 = df.loc[24:34,('average_heartrate')].mean() # preferred method by pandas. but: indeces are different!!
print(f'Average heart rate in selected range: {avgheart:.2f}')


## c) Heart-rate and elevation gain 

# make smaller dataframe with our selected range
smol = df.loc[24:34, ('total_elevation_gain', 'length_km', 'average_heartrate')]

# add elevation gain per km
smol['elevation_gain_km'] = smol.total_elevation_gain / smol.length_km

# heartrate of run with highest elevation gain
# .values[0] needed to get float instead of pandas series as return type!
heartrate_bestelev = smol.loc[smol.elevation_gain_km == max(smol.elevation_gain_km), ('average_heartrate')].values[0]

higher = heartrate_bestelev > avgheart2
print(f'Heartrate of run with highest elevation gain: {heartrate_bestelev}')
print(f'This is above the average heartrate: {higher}')
      





