### Exercise 2 of Lecture 2 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 2. Manipulating arrays

# a) Load bitmap image and change colours

# Import packages
import matplotlib.pyplot as plt
import numpy as np

im = plt.imread('Lecture-2_materials/python.bmp')
im = np.array(im)

plt.imshow(im)

# Assign colour to the pixels
im[50:61,10:21] = np.array([255, 0, 0], dtype = np.uint8)
plt.imshow(im)
plt.show() # Show original image

#-------------------------------------------------------------------------------

# b) Flipping

# Make a copy of the top half of the image
im_copy_top = im[0:128, :].copy()

# Flip the copy along the 2nd axis with np.flip
im_copy_top = np.flip(im_copy_top, axis=1) # axis 0: horizontal, 1: vertical, 2: color
plt.imshow(im_copy_top)
plt.show() # Show modified image

#-------------------------------------------------------------------------------

# c) Splicing

# Make a copy of the bottom half of the image
im_copy_bottom = im[128:255, :].copy()

# Concatenate/splice the top half and bottom half of the image
im_splice = np.concatenate([im_copy_top, im_copy_bottom])
plt.imshow(im_splice)
plt.show() # Show modified image

# Plot the original image and the spliced image side by side
f = plt.figure()
f.add_subplot(1, 2, 1)
plt.imshow(im)
f.add_subplot(1, 2, 2)
plt.imshow(im_splice)
plt.show() # Show plots

#-------------------------------------------------------------------------------

# d) Shallow copy

# Create shallow copy of the top half of the image
im_view = im[0:128, :].view()

# Change every black pixel to red pixel
im_view[np.where((im_view == [0, 0, 0]).all(axis=2))] = [255, 0, 0]

f = plt.figure()
f.add_subplot(1, 2, 1)
plt.imshow(im)
f.add_subplot(1, 2, 2)
plt.imshow(im_view)
plt.show() # Show plots

#-------------------------------------------------------------------------------

# e) Copies and pointers

'''
View is a shallow copy that doesn't have its own data, compared to copy which is a deep copy that stores a copy of the data separately. When using view, modifying the original data affects their views, and vice versa.
'''