'''
Lecture 2 Exercises - example solution.
Note that these examples only represent one of many acceptable ways of solving
the exercises, and the level of sophistication expected at this point in the
course. More sophisticated solutions are always appreciated but not expected.
'''

from scipy.io.wavfile import read
import matplotlib.pyplot as plt
import time
import sounddevice as sd
import numpy as np



''' Exercise 1 '''
fs, sounds = read('./sounds.wav')

# Exercise 1b
sound_len_half = sounds.shape[0] // 2
door, frog = sounds[sound_len_half:], sounds[:sound_len_half]

# Exercise 1c
silence = np.zeros(frog.shape[0], dtype=float)

# Exercise 1d
sound_concat = np.concatenate([frog, silence, frog, silence, door])
sd.play(sound_concat, fs)
time.sleep(2)
plt.plot(sound_concat)
plt.show()



''' Exercise 2 '''

def show_ims(ims, titles):
    n_ims = len(ims)
    for im_i, im in enumerate(ims):
        plt.subplot(1, n_ims, im_i+1)
        plt.title(titles[im_i])
        plt.imshow(im)
    plt.show()

im = plt.imread('./python.bmp')
im = np.array(im)

# Exercise 2a
im[49:60, 9:20] = np.array([255, 0, 0], dtype=np.uint8)
show_ims([im], ['2A'])

# Exercise 2b
im_top_flip = np.flip(im[:im.shape[1] // 2].copy(), axis=1)
show_ims([im_top_flip], ['2B'])

# Exercise 2c
im_bottom = im[im.shape[1] // 2:].copy()
im_splice = np.concatenate([im_top_flip, im_bottom])
show_ims([im_splice, im], ['2C: Splice', '2C: Original'])

# Exercise 2d
im_top = im[:im.shape[1] // 2].view()
top_bl = np.all(im_top[:, :] == [0, 0, 0], axis=2)
im_top[top_bl[:, :]] = [255, 0, 0]
show_ims([im_top.base, im], ['2D: Base', '2D: Original'])



''' Exercise 3 '''

# Exercise 3a
fs, sounds = read('./sounds.wav')
sounds_multi = np.multiply(sounds, 2)

# Exercise 3b
temperatures = np.load('./temperatures.npy', allow_pickle=True)
np.max(temperatures, axis=1)
np.average(temperatures, axis=0)

# Exercise 3c
max_temperature = np.max(temperatures)
max_temperature_idx = np.where(temperatures == max_temperature)