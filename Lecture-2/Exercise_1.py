### Exercise 1 of Lecture 2 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
import numpy as np

## 1. Working with continuous signals

# A) Load and investigate sound

# Load the sound file
from scipy.io.wavfile import read
fs, sounds = read("Lecture-2_materials/sounds.wav") # fs: int, sounds: numpy.ndarray

# (a) Plot the sound signal
import matplotlib.pyplot as plt
plt.plot(sounds)
plt.show()

# (b) Play the sound
import sounddevice as sd
import time
sd.play(sounds, fs)
time.sleep(1)

## B) split sounds into two equal parts
# first half (door)
door = sounds[:int(len(sounds)/2)]
print(len(door)) # check length

# second half (frog)
froggy = sounds[int(len(sounds)/2):]
print(len(froggy)) # check length

## C) create a silent period

silence = np.zeros(len(door))

## D) concatenate the signals

new_sound = np.concatenate((froggy, silence, froggy, silence, door))

# play sound
sd.play(new_sound, fs)

# plot sound
plt.plot(new_sound)
plt.show()
