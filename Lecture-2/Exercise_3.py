### Exercise 3 of Lecture 2 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

# load packages
import numpy as np
from scipy.io.wavfile import read
import sounddevice as sd

# extra info on numpy axes values: https://www.sharpsightlabs.com/blog/numpy-axes-explained/


## 3. Working with arrays
# a) Amplify signal

fs, sounds = read("Lecture-2_materials/sounds.wav")

doubled = np.multiply(sounds, 2)

print(sounds[:10])
print(doubled[:10])

sd.play(sounds, fs)
sd.play(doubled, fs)

# b) temperatures

temperatures = np.load('Lecture-2_materials/temperatures.npy', allow_pickle=True)
temperatures[0] #==> 31 data-points for Bern
temperatures[1] #==> 31 data-points for Zurich
temperatures[2] #==> 31 data-points for Amsterdam
temperatures[3] #==> 31 data-points for Reykjavik
temperatures[4] #==> 31 data-points for Rome
temperatures.shape # (5, 31)

# maximum july temperature for each city
max_city = np.max(temperatures, axis=1) 
print(max_city)

# average temperature across cities each day
avg_day = np.average(temperatures, axis=0) 
print(avg_day)

# c) get the location (indeces) of a given value with numpy.where()

max_all = np.max(temperatures) # 37.584596905331765

loc = np.where(temperatures==max_all) # (array([4]), array([5]))

print(temperatures[4][5]) # 37.584596905331765
print(temperatures[loc]) # [37.58459691] (nice, but why the [] ?)
