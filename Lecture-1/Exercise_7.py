### Exercise 7 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 7. Methods


# a) Find smallest number in list
num_list = [32, 99, 12, 17, 77]

# Sort list in ascending order and return the first element of it
def get_min(num_list):
    min_num = sorted(num_list)[0]
    return min_num

print(get_min(num_list))


# b) Find index of element in list
cards = ["2s","3s","4s","5s","6s","7s","8s","9s","10s","Js","Qs","Ks","As","2h","3h","4h","5h","6h","7h","8h","9h","10h","Jh","Qh","Kh","Ah","2d","3d","4d","5d","6d","7d","8d","9d","10d","Jd","Qd","Kd","Ad","2c","3c","4c","5c","6c","7c","8c","9c","10c","Jc","Qc","Kc","Ac"]

def get_index(mylist, x):
    # linear search, return first hit
    for i in range(len(mylist)):
        if mylist[i] == x:
            return i
    return -1

print(get_index(cards, "5s")) # 3


# c) Remove smallest value from list 
num_list = [98, 67, 26, 99, 89, 12]

mindex = get_index(num_list, get_min(num_list))
my_num = num_list.pop(mindex)

print(my_num) #should print 12


# (d) Sum smallest numbers until sum reaches 100 
num_list = [98, 67, 26, 99, 89, 12]
my_sum = 0

while my_sum <= 100:
    mindex = get_index(num_list, get_min(num_list))
    my_sum += num_list.pop(mindex)
    
print(my_sum, num_list)














