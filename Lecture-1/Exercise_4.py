### Exercise 4 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 4. User interaction

user_name = ''

# If user_name is empty, it asks again. Otherwise, it prints 'Hello' + 'name'
while not user_name:
    user_name = str(input('Enter your name: '))
print(f'Hello {user_name}')