### Exercise 5 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 5. Summing up numbers entered by user

the_sum = 0
in_numb = None

# Sum up the numbers the user enters and print them once the user enters 0 
while not in_numb == 0:
    in_numb = int(input('Enter a number: '))
    the_sum += in_numb
print(the_sum)