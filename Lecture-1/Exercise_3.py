### Exercise 3 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 3. Loops

# a) Sum the numbers

# Create a list of numbers
num_list = [ 32, 99, 12, 17, 77 ]
num_sum = 0

# Sum every item in the list
for num in num_list:
    num_sum += num
print(num_sum)

# b) Sum the numbers

counter = 0

# Calculate the mean using the sum and a counter of each element in the list
for i in num_list:
    counter += 1

mean_num = num_sum / counter
print(mean_num)