### Exercise 6 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 6. PingPong

# Iterate over the numbers from 0 to 99 and print the answer depending on whether it's a multiple of 4, 7, or both
for num in range(100):
    if num % 4 == 0 and num % 7 == 0:
        print('PingPong')
    elif num % 7 == 0:
        print('Pong')
    elif num % 4 == 0:
        print('Ping')