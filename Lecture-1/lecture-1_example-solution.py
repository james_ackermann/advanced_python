'''
Lecture 1 Exercises - example solution.
Not that these examples only represent one of many acceptable ways of solving
the exercises, and the level of sophistication expected at this point in the
course. More sophisticated solutions are always appreciated but not expected.
'''



''' Exercise 1 '''

# Exercise 1a
wadlow_cm = 272
user_cm = 179
if wadlow_cm > user_cm:
    print('You are ', wadlow_cm - user_cm, ' cm lower.')
elif wadlow_cm < user_cm:
    print('I don\'t believe you.')
else:
    print('Wow! You\'re as tall as Mr. Wadlow!')

# Exercise 1b
num_list = [ 32, 99, 12, 17, 77 ]
for num in num_list:
    if num >= 50:
        print(num)
    else:
        print('Too low')



''' Exercise 3 '''
num_list = [ 32, 99, 12, 17, 77 ]

# Exercise 3a
lst_sum = 0
for e in num_list:
    lst_sum += e
print(lst_sum)

# Exercise 3b
lst_sum = 0
lst_cnt = 0
for e in num_list:
    lst_sum += e
    lst_cnt += 1
avg = lst_sum / lst_cnt
print(avg)



''' Exercise 4 '''
name = input('Enter name: > ')
while name == '':
    name = input('No name provided!\nEnter name: > ')
print('Hello, {0}'.format(name))



''' Exercise 5 '''
the_sum = 0
in_numb = None
while not in_numb == 0:
    try:
        in_numb = float(input('Please enter a number (enter \'0\' to quit): '))
        the_sum += in_numb
    except ValueError:
        print('Not a number!')
print(the_sum)



''' Exercise 6 '''
for i in range(100):
    if i % 4 == 0 or i % 7 == 0:
        print('{0}{1}'.format('Ping' * (i % 4 == 0 ), 'Pong' * (i % 7 == 0)))



''' Exercise 7 '''
# Exercise 7a
def get_min(num_list):
    min_num = num_list[0]
    for num in num_list[1:]:
        if min_num > num:
            min_num = num
    return min_num

# Exercise 7b
def get_index(lst, var):
    for i, e in enumerate(lst):
        if e == var: return i
    return -1

# Exercise 7c
num_list = [ 32, 99, 12, 17, 77 ]
def pop_smallest(num_list):
    min_num = get_min(num_list)
    min_num_index = get_index(num_list, min_num)
    return num_list.pop(min_num_index)
print(pop_smallest(num_list))

# Exercise 7d
num_list = [ 32, 99, 12, 17, 77 ]
_sum = 0
while _sum <= 100:
    _sum += pop_smallest(num_list)