### Exercise 2 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 2. Variable assignment
# a) List
'''
print(a)
['x', 'y']
'''

# b) Explanation
'''
The variable a points to a list object. When we set b = a, b points to the same list object. This can be checked with id(a) and id(b).
When we change (mutate) that object, both variables will point us to the now mutated list.
'''

# c) String
'''
Strings are an immutable data type, meaning that they cannot be changed (mutated) in the place they are stored. 
So when we change the string b points to, we actually create a new string object for the changed string and make b point to that.
As a is still pointing to the original string, which was left unchanged, the behaviour differs in comparison to mutable objects (like lists).
'''
