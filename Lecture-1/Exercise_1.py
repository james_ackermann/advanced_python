### Exercise 1 of Lecture 1 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

## 1. Fix the code blocks

# a) Height difference

# Set height variables
wadlow_cm = 272
# user_cm = 179
user_cm = int(input('Please enter your height in cm: '))

# Check the condition if the user_cm is lower or higher than wadlow_cm and print a message
if wadlow_cm > user_cm:
    print(f'You are {wadlow_cm - user_cm} cm lower.')
elif wadlow_cm < user_cm:
    print('I don\'t believe you.')
else:
    print('Please input a valid number.')

# b) Indentation

# Create list with numbers
num_list = [32, 99, 12, 17, 77]

# Iterates numbers in the list and prints number if it's great or equal to 50. Otherwise it prints "too low"
for num in num_list:
    if num >= 50:
        print(num)
    else:
        print('Too low')