### Exercise 1 of Lecture 3 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
import numpy as np

## 1. Stroke dataset

# a) Load the dataset

data = np.load('stroke.npy', allow_pickle=True)
gender = data[:, 0]
age = data[:, 1] # Integer value
hypertension = data[:, 2] # [ 0 = No, 1 = Yes ]
heart_disease = data[:, 3] # [ 0 = No, 1 = Yes ]
ever_married = data[:, 4] # [ 0 = No, 1 = Yes ]
work_type = data[:, 5] # [ 0 = Private, 1 = Self-employed, 2 = Never worked, 3 = Government, 4 = Children ]
residence_type = data[:, 6] # [ 0 = Urban, 1 = Rural ]
glucose_level = data[:, 7] # Integer value
bmi = data[:, 8] # Integer value
smoking_status = data[:, 9] #  [0 = Never smoked, 1 = Formerly smoked, 2 = Smokes ]
stroke = data[:, 10] # [ 0 = No, 1 = Yes ]


# b) Patient mask
# Create masks for individuals with and without stroke
patient_mask = stroke == 1
control_mask = stroke == 0

# Calculate sum of individuals 
patient_number = np.sum(patient_mask)
control_number = np.sum(control_mask)
total_number = len(stroke)

print(f'Number of stroke patients: {patient_number}')
print(f'Number of control patients: {control_number}')
print(f'Total number of patients: {total_number}')

print(70*'-')


# c) Calculate mean age of patients and controls
# Use mask to select age and find its mean for the group
stroke_age_mean = int(np.mean(age[patient_mask]))
control_age_mean = int(np.mean(age[control_mask]))

print(f'Mean age of stroke patients: {stroke_age_mean}')
print(f'Mean age of control patients: {control_age_mean}')

print(70*'-')


# d) BMI and heart disease in sub-groups
# Create masks for urban and rural smokers and non-smokers
urban_smokers_mask = (smoking_status == 2) & (residence_type == 0)
urban_nonsmokers_mask =  (smoking_status == (0 | 1)) & (residence_type == 0)
rural_smokers_mask = (smoking_status == 2) & (residence_type == 1)
rural_nonsmokers_mask =  (smoking_status == (0 | 1)) & (residence_type == 1)

# Calculate average BMI and heart disease percentage for urban smokers
bmi_urban_smokers = int(np.mean(bmi[urban_smokers_mask]))
heart_urban_smokers = np.sum(heart_disease[urban_smokers_mask])
print(f'Urban smokers: average BMI = {bmi_urban_smokers} and heart disease = {heart_urban_smokers / np.sum(urban_smokers_mask) * 100 :.1f}%')

# Calculate average BMI and heart disease percentage for urban non-smokers
bmi_urban_nonsmokers = int(np.mean(bmi[urban_nonsmokers_mask]))
heart_urban_nonsmokers = np.sum(heart_disease[urban_nonsmokers_mask])
print(f'Urban non-smokers: average BMI = {bmi_urban_nonsmokers} and heart disease = {heart_urban_nonsmokers / np.sum(urban_nonsmokers_mask) * 100 :.1f}%')

# Calculate average BMI and heart disease percentage for rural smokers
bmi_rural_smokers = int(np.mean(bmi[rural_smokers_mask]))
heart_rural_smokers = np.sum(heart_disease[rural_smokers_mask])
print(f'Rural smokers: average BMI = {bmi_rural_smokers} and heart disease = {heart_rural_smokers / np.sum(rural_smokers_mask) * 100 :.1f}%')

# Calculate average BMI and heart disease percentage for rural non-smokers
bmi_rural_nonsmokers = int(np.mean(bmi[rural_nonsmokers_mask]))
heart_rural_nonsmokers = np.sum(heart_disease[rural_nonsmokers_mask])
print(f'Rural non-smokers: average BMI = {bmi_rural_nonsmokers} and heart disease = {heart_rural_nonsmokers / np.sum(rural_nonsmokers_mask) * 100 :.1f}%')
