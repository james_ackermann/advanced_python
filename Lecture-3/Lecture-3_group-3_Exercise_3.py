### Exercise 3 of Lecture 3 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
import numpy as np

## 3. Age

# Load the dataset and assign variable for each property
data = np.load('stroke.npy', allow_pickle=True)
gender = data[:, 0]
age = data[:, 1] # Integer value
hypertension = data[:, 2] # [ 0 = No, 1 = Yes ]
heart_disease = data[:, 3] # [ 0 = No, 1 = Yes ]
ever_married = data[:, 4] # [ 0 = No, 1 = Yes ]
work_type = data[:, 5] # [ 0 = Private, 1 = Self-employed, 2 = Never worked, 3 = Government, 4 = Children ]
residence_type = data[:, 6] # [ 0 = Urban, 1 = Rural ]
glucose_level = data[:, 7] # Integer value
bmi = data[:, 8] # Integer value
smoking_status = data[:, 9] #  [0 = Never smoked, 1 = Formerly smoked, 2 = Smokes ]
stroke = data[:, 10] # [ 0 = No, 1 = Yes ]


# Sort all the individuals in the dataset by age
age_sorted = np.sort(age)

# Find the age of the 900th youngest individual in the dataset
youngest_900 = age_sorted[899]

# Extract all individuals of the same age as the 900th youngest individual
data_as_old = np.array(data[age == youngest_900])

# Print the age of the 900th youngest individual and the number of individuals of this age
print(f'The 900th youngest individual is {youngest_900} years old, and there are {len(data_as_old)} individuals of the same age.')