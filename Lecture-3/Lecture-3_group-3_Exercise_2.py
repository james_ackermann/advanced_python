### Exercise 2 of Lecture 3 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3
import numpy as np

## 2. BMI

# Load the dataset and assign variable for each property
data = np.load('stroke.npy', allow_pickle=True)
gender = data[:, 0]
age = data[:, 1] # Integer value
hypertension = data[:, 2] # [ 0 = No, 1 = Yes ]
heart_disease = data[:, 3] # [ 0 = No, 1 = Yes ]
ever_married = data[:, 4] # [ 0 = No, 1 = Yes ]
work_type = data[:, 5] # [ 0 = Private, 1 = Self-employed, 2 = Never worked, 3 = Government, 4 = Children ]
residence_type = data[:, 6] # [ 0 = Urban, 1 = Rural ]
glucose_level = data[:, 7] # Integer value
bmi = data[:, 8] # Integer value
smoking_status = data[:, 9] #  [0 = Never smoked, 1 = Formerly smoked, 2 = Smokes ]
stroke = data[:, 10] # [ 0 = No, 1 = Yes ]

control_group_average = np.average(bmi[stroke==0])

# a) stroke patients and bmi

# Has stroke and BMI above the control group average
above = (stroke == 1) & (bmi > control_group_average)
below = (stroke == 1) & (bmi <= control_group_average)

## Print number and percentage of stroke patients with BMI above the average in the control group
# percentage of stroke patients -> 100% is 180

total_stroke_patients = np.sum(stroke==1)
print(f'Number of stroke patients with BMI > control: {np.sum(above)}')
print(f'Percentage of stroke patients with BMI > control: {(np.sum(above)/total_stroke_patients*100):.2f}')

print('-'*70)

# note: the dataset is sorted, stroke patients are the first entries [0...179]!
ind_above = np.where(above)
ind_below = np.where(below)


# b) BMI, stroke and heart disease

heart_above = np.sum(heart_disease[ind_above])
print(f'Number of stroke patients with heart disease and BMI > control: {heart_above}')

heart_below = np.sum(heart_disease[ind_below])
print(f'Number of stroke patients with heart disease and BMI <= control: {heart_below}')





