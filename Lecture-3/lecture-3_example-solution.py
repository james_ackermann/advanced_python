'''
Lecture 3 Exercises - example solution.
Note that these examples only represent one of many acceptable ways of solving
the exercises, and the level of sophistication expected at this point in the
course. More sophisticated solutions are always appreciated but not expected.
'''

import numpy as np



''' Exercise 1 '''

# Exercise 1a
data            = np.load('./stroke.npy', allow_pickle=True)
gender          = data[:, 0]    # [ 0 = Male, 1 = Female , 2 = Other] => Categorical
age             = data[:, 1]    # Integer value => Continuous
hypertension    = data[:, 2]    # [ 0 = No, 1 = Yes ] => Categorical
heart_disease   = data[:, 3]    # [ 0 = No, 1 = Yes ] => Categorical
ever_married    = data[:, 4]    # [ 0 = No, 1 = Yes ] => Categorical
work_type       = data[:, 5]    # [ 0 = Private, 1 = Self-employed,
                                #   2 = Never worked, 3 = Government,
                                #   4 = Children ] => Categorical
residence_type  = data[:, 6]    # [ 0 = Urban, 1 = Rural ] => Categorical
glucose_level   = data[:, 7]    # Integer value => Continuous
bmi             = data[:, 8]    # Integer value => Continuous
smoking_status  = data[:, 9]    # [ 0 = Never smoked, 1 = Formerly smoked,
                                #   2 = Smokes ] => Categorical
stroke          = data[:, 10]   # [ 0 = No, 1 = Yes ] => Categorical

# Exercise 1b
patient_mask = stroke
control_mask = np.logical_not(stroke)
print('Patients: N = {n}'.format(n=np.sum(patient_mask)))
print('Controls: N = {n}'.format(n=np.sum(control_mask)))

# Exercise 1c
patient_Mage = np.mean(age[patient_mask])
control_Mage = np.mean(age[control_mask])
print('Patients: M age = {m}'.format(m=int(round(patient_Mage, 0))))
print('Controls: M age = {m}'.format(m=int(round(control_Mage, 0))))

# Exercise 1d
hd = (heart_disease == 1)

for r, res_code in zip(['Urban', 'Rural'], [0, 1]):
    for s, smoke_code in zip(['smokers', 'non-smokers'], [[2], [0,1]]):
        
        mask = ((residence_type == res_code) & \
            np.any(np.array([smoking_status == c for c in smoke_code]), axis=0))
        
        m = round(np.mean(bmi[mask]), 2)
        p = round(np.sum((mask & hd)) / np.sum(mask) * 100, 2)
        
        print(f'{r} {s.ljust(15)} M BMI = {str(m).ljust(10)} HD = {p} %')



''' Exercise 2 '''

# Exercise 2a
control_Mbmi = np.mean(bmi[control_mask])
patient_bmiA_mask = stroke & (bmi > control_Mbmi)
patient_bmiB_mask = stroke & (bmi <= control_Mbmi)

print('Patients with BMI above control-group average: N = {n}, {prct} %'.format(
    n=np.sum(patient_bmiA_mask),
    prct=round(np.sum(patient_bmiA_mask) / np.sum(stroke) * 100, 2)
))

bmiA_idx = np.where(patient_bmiA_mask)
bmiB_idx = np.where(patient_bmiB_mask)

# Exercise 2b
print('N HD patients with BMI > average: {n}'.format(n=np.sum(hd[bmiA_idx])))
print('N HD patients with BMI <= average: {n}'.format(n=np.sum(hd[bmiB_idx])))



''' Exercise 3 '''

data_sorted_by_age = data[np.argsort(age)]
age_900th = data_sorted_by_age[899, 1]
data_as_old = data[age == age_900th]
print('Age of 900th youngest = {age}'.format(age=age_900th))
print('N of the same age = {n}'.format(n=data_as_old.shape[0]))