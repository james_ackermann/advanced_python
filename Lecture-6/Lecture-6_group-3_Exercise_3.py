### Exercise 3 of Lecture 6 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


## 3. COVID-19 Vaccination
# Load data
df = pd.read_csv('./materials/vaccination.csv')

# Apply masks for countries
ch = df['country'] == 'Switzerland'
de = df['country'] == 'Germany'
fr = df['country'] == 'France'
id = df['country'] == 'India'

# Calculate population of country per 10k
ch_pop_10k = 8637000 / 100000
de_pop_10k = 83240000 / 100000
fr_pop_10k = 67390000 / 100000
id_pop_10k = 1380000000 / 100000

# Change column date to be timestamp
df['date'] = df['date'].apply(pd.Timestamp)

# Set first day to be 2021-01-01
first_day = pd.to_datetime('20210101', format='%Y%m%d')

# Get days since vaccination start
df['days'] = (df['date'] - first_day).dt.days

# Set size of plots
plt.figure(figsize=(7, 8))

# Create subplots
plt.subplot(2, 1, 1)

# Plot daily vaccination per country
plt.plot(df[ch]['days'], (df[ch]['daily_vaccinations'] / ch_pop_10k))
plt.plot(df[de]['days'], (df[de]['daily_vaccinations'] / de_pop_10k))
plt.plot(df[fr]['days'], (df[fr]['daily_vaccinations'] / fr_pop_10k))
plt.plot(df[id]['days'], (df[id]['daily_vaccinations'] / id_pop_10k))

# Set x limit
plt.xlim(0, 250)

# Set labels
plt.xlabel('Days since vaccination campaign start')
plt.ylabel('Per 100 000 people')
plt.title('Daily vaccinations')

# Set legend
leg = plt.legend(['Switzerland', 'Germany', 'France', 'India'])
leg.get_frame().set_linewidth(0.0)


# Get world-wide mean and standard error of mean
world_mean = df.groupby(['days']).mean().reset_index()
world_sem = df.groupby(['days']).sem().reset_index()

# Set upper and lower limits of the mean +/- standard error of mean
upper_lim = world_mean['daily_vaccinations'] + world_sem['daily_vaccinations']
lower_lim = world_mean['daily_vaccinations'] - world_sem['daily_vaccinations']

plt.subplot(2, 1, 2)

# Plot figure and fill the SEM inbetween
plt.plot(world_mean['days'], world_mean['daily_vaccinations'], c='grey')
plt.fill_between(world_mean['days'], upper_lim, lower_lim, alpha=0.1, color='black')

# Set x limit
plt.xlim(0, 250)
plt.ylim(0, 330000)

# Set labels
plt.xlabel('Days since vaccination campaign start')
plt.ylabel('Number of vaccinations')
plt.title('Mean daily vaccinations world-wide (Shaded area SEM)')

# Adjust layout
plt.tight_layout()

# Save figure to PDF format
plt.savefig('COVID-19_vaccination.pdf', )
plt.show()
