### Exercise 2 of Lecture 6 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import pandas as pd
import matplotlib.pyplot as plt

## 2. Attributes of top songs on Spotify

# Load the data
spotify = pd.read_csv('./materials/spotify.csv')

# Specify the axes
x_axis = spotify['energy']
y_axis_l = spotify['valence']
y_axis_r = spotify['liveness']


#plt.figure(figsize=(10,5))
# Plot the scatter plot with colour map
plt.scatter(x_axis, y_axis_l, s=100, c=y_axis_r, cmap='viridis', alpha=0.5)

# Set limits
plt.xlim(30, 90)
plt.ylim(0, 100)

# Set labels
plt.xlabel('Energy')
plt.ylabel('Valence')
plt.title('Attributes of top 50 songs on Spotify in 2019')

cbar = plt.colorbar()
cbar.set_label('Liveness')

plt.show()

