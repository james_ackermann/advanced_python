### Exercise 1 of Lecture 6 for Advanced Python - Rafel Al Doori and James Ackermann, Group 3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_style('ticks')

## 1. Titanic
'''
You will find a CSV file named titanic.csv in the materials for this exercise set. Use this file
to solve Exercises 1a and 1b. (Credit: https://www.kaggle.com/heptapod/titanic).
'''

titanic = pd.read_csv('./materials/titanic.csv', index_col=0)
print(titanic.head(10))

# a) Fare prices by class
'''
Reproduce the figure below, which show the distribution of fare prices for first and second
class passengers, using seaborn.
'''

##### histplot try without loop (not too good, proportions end up wrong, legend not better..)
'''
# smaller dataset (only the info we want)
smol_titanic = titanic[titanic['Pclass']!=1][['Fare', 'Pclass']]

palette=['forestgreen', 'red']

sns.histplot(smol_titanic, x='Fare', hue='Pclass', stat='probability',
            palette=palette, kde=True, 
            kde_kws={'cut':3, 'bw_method':0.3}, linewidth=0, common_bins=False)
plt.legend(['Second Class', 'Third Class'])
'''


##### histplot with loop, okay except for legend

def helpme(case):
    # bw_adjust to fit the given figure (separate for each class)
    if case==3:
        # have to specify cut to get the x-axis and kde-lines right
        return {'cut':3,'bw_adjust':0.51}
    else:
        return {'cut':3,'bw_method':0.3}

i=0
palette=['', '', 'forestgreen', 'red']

for pclass in np.sort(titanic['Pclass'].unique()):
    if pclass == 1:
        continue
    else:
        sns.histplot(titanic[titanic['Pclass']==pclass], x='Fare', 
                     multiple="layer", stat='density',
                     kde=True, kde_kws=helpme(pclass),
                     color=palette[pclass], linewidth=0)
        i+=1
        plt.legend(['Second Class', 'Third Class'], frameon=False, borderpad=0.8)
        plt.title('Titanic fare prices by class')
        plt.ylabel('Proportion of passengers')


##### soon deprecated distplot, also with a loop, colors fit slightly better, otherwise same

'''
def helpme(case):
    # bw_adjust to fit the given figure (separate for each class)
    if case==3:
        # have to specify cut to get the x-axis and kde-lines right
        return {'bw_adjust':0.51}
    else:
        return {'bw_method':0.3}


palette=['', '', 'forestgreen', 'red']

for pclass in np.sort(titanic['Pclass'].unique()):
    if pclass != 1:
        sns.distplot(titanic[titanic['Pclass']==pclass]['Fare'], 
                     kde_kws=helpme(pclass), hist_kws={'linewidth':0}, 
                     color=palette[pclass])
plt.legend(['Second Class', 'Third Class'], frameon=False, borderpad=0.8)
plt.title('Titanic fare prices by class')
plt.ylabel('Proportion of passengers')
'''


#-------------------b) Outcome by fare-price and age
palette=['forestgreen', 'red']
sns.scatterplot(data=titanic, x='Age', 
                y='Fare', 
                hue='Survived', 
                palette=palette, alpha=0.5) #edgecolor by group (with higher alpha)???
plt.xlim(0, 70)
plt.ylim(0,250)
plt.margins(0.2) # this does nothing :/
plt.xlabel('Age')
plt.ylabel('Fare')
plt.legend(['Survived', 'Died'], loc='upper left')
plt.title('Passenger outcome by fare-price and age')

# again, legend markers...






