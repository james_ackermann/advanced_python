# EEG Data Analysis

Rafel Al-Doori and James Ackermann

## Mission
Analyze a person's EEG responses to two different auditory stimuli.

## Summary
We are given a raw EEG recording of one channel (sampled at 500Hz). The participant was presented with two different auditory stimuli at different timepoints during the recording. These timepoints are also given in a separate file.

From these files, we extract the participant's responses to each event and analyze them to see whether they show a response (the signal differs significantly from the baseline) and how the responses differ according to stimulus.

## Milestones

### Part 1:
**Code Task 1:**
For each type of auditory stimulus, create an array which containins the EEG signals from 0.3 seconds before to 1 second after this types stimuli. 
General form: `stimulusA = [[signals_A_1],[signals_A_2],[signals_A_3],...]`
* [x] Create Python Script
* [x] Load given files and get an overview
* [x] Create function to extract signals in the given timeframe around an event
* [x] Create arrays containing all EEG signal responses for each of the stimulus types

**README.md:**
* [x] set up README.md
* [x] add known dependencies

**ROADMAP.md:**
* [x] set up ROADMAP.md
* [x] add Mission
* [x] add Summary
* [x] add Milestones
* [x] add Deadlines

### Part 2:
**Code Task 2:**
Plot the average response for each type of event, with shading for the standard error. On the plot, also indicate t=0, the amplitude of the signal in microvolts (Y-axis), and the time relative to sound onset in ms (X-axis).
- [ ] code task 2
	- [ ] get averages, store in appropriate datatype (pandas df?)
	- [ ] get standard errors, store in appropriate datatype
	- [ ] plot average response according to task description
- [ ] update README.md
- [ ] create repository on AdvPython Github
- [ ] add code (incl. files to run it), README and ROADMAP to the repository

### Part 3:
- [ ] improve code using pylint feedback
- [ ] refactor code to repository
- [ ] re-implement code to contain at least one (meaningful) class
- [ ] implement at least one use of the class(es) in code-base
- [ ] update readme

### Part 4:
- [ ] implement at least one set of try-except blocks
- [ ] implement a meaningful unit test with at least four test cases
- [ ] update readme
- [ ] provide feedback to another group in the form of an issue on their project


### Part 5:
**Code Task 3:**
For each sound type, for each time-point after sound onset, test whether it is significantly different across trials from the baseline. Also test at what time-points after sound onset the EEG signals for the two sound-types are different from each other. Produce one plot containing the average EEG signal of each sound type, along with lines indicating the significant time-periods.
- [ ] code task 3
- [ ] set up a virtual environment that can run project
- [ ] prepare presentation

### Presentation:
* [ ] choose code examples to present
* [ ] present findings
- [ ] create slides
- [ ] divide presentation between the two of us

## Deadlines

| **Deadline** | **Date** |
| --- | --- |
|  Hand-In Task 1   |   28.04.2022, 14:00  |
|  Hand-In Task 2   |   05.05.2022, 14:00  |
|  Hand-In Task 3   |   12.05.2022, 14:00  |
|  Hand-In Task 4   |   19.05.2022, 14:00  |
|  Hand-In Task 5   |   02.06.2022, 14:00  |
|  5m-Presentation  |   02.06.2022, 14-16  |


