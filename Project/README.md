# EEG-Project ReadMe

This is a collaborative project between students for the course of Advanced Python to learn about and apply coding project management.

We use raw electroencephalography (EEG) recordings, sampled at 500 Hz, to analyse the responses of a participant to two different auditory stimuli.

## How to run
No arguments are needed to run the script.

Simple write the following in the terminal:
```
python3 task1.py
```

## Dependencies
Python 3 environment is needed to run the following packages:

- numpy, 1.21.2
- pandas, 1.3.3
- matplotlib, 3.4.3
