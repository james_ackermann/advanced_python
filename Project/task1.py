#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np # v1.21.2
import pandas as pd # v1.3.3
import matplotlib.pyplot as plt # v3.4.3

## load the data
eeg = np.load("EEG/eeg.npy", allow_pickle=True)
events = np.load("EEG/events.npy", allow_pickle=True)

## helper function(s)
def extract_response(event_index):
    '''
    ===== from seconds to signals =====
    --- sampled at 500Hz, thus:
    1s = 500 signals
    0.3s = 150 signals
    '''
    resp = eeg[event_index-150:event_index+500]
    return resp


## get an overview, what does the data look like?

# eeg: sampled at 500hz, so 500 datapoints/s!
print(eeg.shape)  # (2287366,)
print(eeg[:10])

plt.plot(eeg[:500]) # plot of the first second
plt.show()

plt.plot(eeg[-501:]) # plot of the last second
plt.show()

# events:
print(events.shape) # (1440, 2)
print(events[:, 0]) # when events happened (at which datapoints)
print(events[:, 1]) # which event happened (0 or 1)



# extract filtered events
events0 = events[events[:, 1] == 0]
events1 = events[events[:, 1] == 1]

# prep filtered responses
responses0 = np.zeros((720, 650))
responses1 = np.zeros((720, 650))

# extract responses into prepared arrays
for i in range(len(events0)):
    event = events0[i]
    responses0[i] = extract_response(event[0])
    
for i in range(len(events1)):
    event = events1[i]
    responses1[i] = extract_response(event[0])









